﻿Imports System.Math

Public Class FFT
    Dim pi As Double
    Dim vector() As Double

    Sub CFourier()
        pi = 4 * Atan(CDbl(1))
        vector = Nothing
    End Sub
    Sub CFourier2()
        If vector IsNot Nothing Then
            'delete [] vector;
            ReDim vector(-1)
        End If
    End Sub

    ' FFT 1D
    Sub ComplexFFT(ByVal data() As Double, ByVal number_of_samples As Integer, ByVal sample_rate As Integer, ByVal sign As Integer)
        Dim n, mmax, m, j, istep, i As UInt64
        Dim wtemp, wr, wpr, wpi, wi, theta, tempr, tempi As Double

        ReDim vector(2 * sample_rate - 1) 'set last index, not array size

        For n = 0 To sample_rate - 1
            If n < number_of_samples Then
                vector(2 * n) = data(n)
            Else
                vector(2 * n) = 0
            End If
            vector(2 * n + 1) = 0
        Next

        n = sample_rate << 1
        j = 0
        For i = 0 To n / 2 - 1 Step 2
            If j > i Then
                Swap(vector(j), vector(i))
                Swap(vector(j + 1), vector(i + 1))
                If j / 2 < n / 4 Then
                    Swap(vector(n - (i + 2)), vector(n - (j + 2)))
                    Swap(vector((n - (i + 2)) + 1), vector((n - (j + 2)) + 1))
                End If
            End If
            m = n >> 1
            While m >= 2 And j >= m
                j -= m
                m >>= 1
            End While
            j += m
        Next

        mmax = 2
        While n > mmax
            istep = mmax << 1
            theta = sign * (2 * pi / mmax)
            wtemp = Sin(0.5 * theta)
            wpr = -2.0 * wtemp * wtemp
            wpi = Sin(theta)
            wr = 1.0
            wi = 0.0
            For m = 1 To mmax - 1 Step 2
                For i = m To n Step istep
                    j = i + mmax
                    tempr = wr * vector(j - 1) - wi * vector(j)
                    tempi = wr * vector(j) + wi * vector(j - 1)
                    vector(j - 1) = vector(i - 1) - tempr
                    vector(j) = vector(i) - tempi
                    vector(i - 1) += tempr
                    vector(i) += tempi
                Next
                wr = (wtemp = wr) * wpr - wi * wpi + wr
                wi = wi * wpr + wtemp * wpi + wi
            Next
            mmax = istep
        End While

        Dim fundamental_frequency As Long
        fundamental_frequency = 0
        For i = 2 To sample_rate Step 2
            If (Pow(vector(i), 2) + Pow(vector(i + 1), 2)) > (Pow(vector(fundamental_frequency), 2) + Pow(vector(fundamental_frequency + 1), 2)) Then
                fundamental_frequency = i
            End If
        Next
        fundamental_frequency = CLng(CDbl(fundamental_frequency / 2))
    End Sub

    Sub Swap(ByRef a As Object, ByRef b As Object)
        Dim t As Object = a
        a = b
        b = t
    End Sub


    '  11  #include "StdAfx.h"
    '  12  #include <math.h>
    '  13  #include ".\fourier.h"
    '  14  
    '  15  #define SWAP(a,b) tempr=(a);(a)=(b);(b)=tempr
    '  16  
    '  17  CFourier::CFourier(void)
    '  18  {
    '  19      pi=4*atan((double)1);vector=NULL;
    '  20  }
    '  21  
    '  22  CFourier::~CFourier(void)
    '  23  {if(vector!=NULL)
    '  24          delete [] vector;
    '  25  }
    '  26  
    '  27  // FFT 1D
    '  28  void CFourier::ComplexFFT(float data[], unsigned long number_of_samples, unsigned int sample_rate, int sign)
    '  29  {
    '  30  
    '  31      //variables for the fft
    '  32      unsigned long n,mmax,m,j,istep,i;
    '  33      double wtemp,wr,wpr,wpi,wi,theta,tempr,tempi;
    '  34  
    '  35      //the complex array is real+complex so the array 
    '  36      //as a size n = 2* number of complex samples
    '  37      //real part is the data[index] and 
    '  38      //the complex part is the data[index+1]
    '  39  
    '  40      //new complex array of size n=2*sample_rate
    '  41      if(vector!=NULL)
    '  42          delete [] vector;
    '  43  
    '  44      vector=new float [2*sample_rate];
    '  45  
    '  46      //put the real array in a complex array
    '  47      //the complex part is filled with 0's
    '  48      //the remaining vector with no data is filled with 0's
    '  49      for(n=0; n<sample_rate;n++)
    '  50      {
    '  51          if(n<number_of_samples)
    '  52              vector[2*n]=data[n];
    '  53          else
    '  54              vector[2*n]=0;
    '  55          vector[2*n+1]=0;
    '  56      }
    '  57  
    '  58      //binary inversion (note that the indexes 
    '  59      //start from 0 witch means that the
    '  60      //real part of the complex is on the even-indexes 
    '  61      //and the complex part is on the odd-indexes)
    '  62      n=sample_rate << 1;
    '  63      j=0;
    '  64      for (i=0;i<n/2;i+=2) {
    '  65          if (j > i) {
    '  66              SWAP(vector[j],vector[i]);
    '  67              SWAP(vector[j+1],vector[i+1]);
    '  68              if((j/2)<(n/4)){
    '  69                  SWAP(vector[(n-(i+2))],vector[(n-(j+2))]);
    '  70                  SWAP(vector[(n-(i+2))+1],vector[(n-(j+2))+1]);
    '  71              }
    '  72          }
    '  73          m=n >> 1;
    '  74          while (m >= 2 && j >= m) {
    '  75              j -= m;
    '  76              m >>= 1;
    '  77          }
    '  78          j += m;
    '  79      }
    '  80      //end of the bit-reversed order algorithm
    '  81  
    '  82      //Danielson-Lanzcos routine
    '  83      mmax=2;
    '  84      while (n > mmax) {
    '  85          istep=mmax << 1;
    '  86          theta=sign*(2*pi/mmax);
    '  87          wtemp=sin(0.5*theta);
    '  88          wpr = -2.0*wtemp*wtemp;
    '  89          wpi=sin(theta);
    '  90          wr=1.0;
    '  91          wi=0.0;
    '  92          for (m=1;m<mmax;m+=2) {
    '  93              for (i=m;i<=n;i+=istep) {
    '  94                  j=i+mmax;
    '  95                  tempr=wr*vector[j-1]-wi*vector[j];
    '  96                  tempi=wr*vector[j]+wi*vector[j-1];
    '  97                  vector[j-1]=vector[i-1]-tempr;
    '  98                  vector[j]=vector[i]-tempi;
    '  99                  vector[i-1] += tempr;
    ' 100                  vector[i] += tempi;
    ' 101              }
    ' 102              wr=(wtemp=wr)*wpr-wi*wpi+wr;
    ' 103              wi=wi*wpr+wtemp*wpi+wi;
    ' 104          }
    ' 105          mmax=istep;
    ' 106      }
    ' 107      //end of the algorithm
    ' 108      
    ' 109      //determine the fundamental frequency
    ' 110      //look for the maximum absolute value in the complex array
    ' 111      fundamental_frequency=0;
    ' 112      for(i=2; i<=sample_rate; i+=2)
    ' 113      {
    ' 114          if((pow(vector[i],2)+pow(vector[i+1],2))>(pow(vector[fundamental_frequency],2)+pow(vector[fundamental_frequency+1],2))){
    ' 115              fundamental_frequency=i;
    ' 116          }
    ' 117      }
    ' 118  
    ' 119      //since the array of complex has the format [real][complex]=>[absolute value]
    ' 120      //the maximum absolute value must be ajusted to half
    ' 121      fundamental_frequency=(long)floor((float)fundamental_frequency/2);
    ' 122  
    ' 123  }
End Class
