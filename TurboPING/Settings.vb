﻿Module Settings
    Sub LoadSettings()
        Try
            FormMain.Left = My.Settings("PosX")
            FormMain.Top = My.Settings("PosY")
            FormMain.Width = My.Settings("SizeX")
            FormMain.Height = My.Settings("SizeY")
            FormMain.PingSpeedControl.Value = My.Settings("Speed")
            Ping.Address = My.Settings("Address")
        Catch ex As Exception
            ' tough luck
        End Try
    End Sub

    Sub SaveSettings()
        ' reset form state
        If FormMain.FormBorderStyle = Windows.Forms.FormBorderStyle.None Then FormMain.SwitchFormState()
        My.Settings("PosX") = FormMain.Left
        My.Settings("PosY") = FormMain.Top
        My.Settings("SizeX") = FormMain.Width
        My.Settings("SizeY") = FormMain.Height
        My.Settings("Speed") = FormMain.PingSpeedControl.Value
        My.Settings("Address") = Ping.Address
    End Sub
End Module
