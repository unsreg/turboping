﻿Imports System.Net.NetworkInformation

Module Ping
    Public PingStarted As DateTime = Now
    Public PingTimes As New List(Of Single)
    Public Address As String = "8.8.4.4"
    Private Buffer(1) As Byte 'size 1472 is most optimal

    Dim LastPings(0) As Integer
    Sub Measure()
        Dim a As New System.Net.NetworkInformation.Ping
        Dim r As System.Net.NetworkInformation.PingReply
        ' a tracert to google.com revealed this to be the closest internet hop
        ' but google.com works too...
        Try
            Dim _Then As DateTime = Now
            r = a.Send(Address, 1000, Buffer)
            Dim _Delay As TimeSpan = Now - _Then

            ' Gaussian smooth
            If LastPings.Length = 1 Then ReDim LastPings(GaussWeights.Length * 2 - 1)
            For i As Integer = 0 To LastPings.Length - 2
                LastPings(i) = LastPings(i + 1)
            Next
            LastPings(LastPings.Length - 1) = IIf(r.RoundtripTime > 0, r.RoundtripTime, _Delay.TotalMilliseconds)
            PingTimes.Add(LowPassFilter(LastPings))

            'PingTimes.Add(IIf(r.RoundtripTime > 0, r.RoundtripTime, _Delay.TotalMilliseconds))
        Catch ex As Exception
        End Try
    End Sub

    Function Test(ByVal NewAddress) As Boolean
        Dim a As New System.Net.NetworkInformation.Ping
        Dim r As System.Net.NetworkInformation.PingReply
        Try
            r = a.Send(NewAddress)
            PingStarted = Now
            PingTimes.Clear()
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Dim GaussWeights() As Double = {1.0, 0.98019867331, 0.92311634639, 0.83527021141, 0.72614903707, 0.60653065971, 0.48675225596, 0.37531109885, 0.27803730045, 0.19789869908, 0.13533528324, 0.088921617459, 0.056134762834, 0.034047454735, 0.019841094744, 0.011108996538, 0.005976022895, 0.0030887154082, 0.0015338106793, 0.00073180241888}
    Function LowPassFilter(ByVal Last8() As Integer) As Single
        Dim Dbl As Double = 0
        Dim j As Integer = 0
        For i As Integer = GaussWeights.Length - 1 To 1 Step -1
            Dbl += Last8(j) * GaussWeights(i)
            j += 1
        Next
        For i As Integer = 0 To GaussWeights.Length - 1
            Dbl += Last8(j) * GaussWeights(i)
            j += 1
        Next
        Return Dbl / (GaussWeights.Sum * 2 - 1)
    End Function
End Module
