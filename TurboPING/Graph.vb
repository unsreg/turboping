﻿Imports System.Drawing

Module Graph
    Public Dimensions As Drawing.Rectangle
    Public Sub UpdateDimensions()
        Dimensions = FormMain.GraphBox.DisplayRectangle
        _GraphBitmap = New Bitmap(Dimensions.Width, Dimensions.Height, Drawing.Imaging.PixelFormat.Format24bppRgb)
        _Graphics = Graphics.FromImage(_GraphBitmap)
        _Graphics.CompositingMode = Drawing2D.CompositingMode.SourceOver ' applies alpha transparency when drawing

        _Graphics.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
        _Graphics.CompositingQuality = Drawing2D.CompositingQuality.AssumeLinear ' smooth anti-aliasing, more accurate single precision positioning?
        _Graphics.PixelOffsetMode = Drawing2D.PixelOffsetMode.None
    End Sub

#Region "BITMAP & GRAPHIC"
    Private _GraphBitmap As Bitmap
    Private _Graphics As Graphics
#End Region

#Region "Style"
    ' Background
    Private BGColor As Color = Color.FromArgb(127, 0, 0, 0)
    Private BGSlowFade As Color = Color.FromArgb(15, 0, 0, 0)
    Private BGSlowFadeBrush As New SolidBrush(BGSlowFade)
    Private BGSlowFadeAlert As Color = Color.FromArgb(127, 255, 0, 0)
    Private BGSlowFadeAlertBrush As New SolidBrush(BGSlowFadeAlert)

    ' Ping Font
    Private PingFont As New Font("Consolas", CSng(9))
    Private PingBrush As New SolidBrush(Color.FromArgb(255, 15, 95, 0))

    ' Graph Style
    Private GraphPen As New Pen(New SolidBrush(Color.FromArgb(255, 255, 190, 50)), 1)
    Private AvgPen As New Pen(New SolidBrush(Color.FromArgb(255, 255, 255, 0)), 2)

    ' Fading line-style
    'Private CurrentPen As New Pen(New SolidBrush(Color.FromArgb(191, 255, 223, 31)), 1.0) ' Amber color
    Private CurrentPen As New Pen(New SolidBrush(Color.FromArgb(255, 255, 255, 255)), 1.0) ' White color 

    Private GridPen As New Pen(New SolidBrush(Color.FromArgb(31, 255, 255, 255)), 1)
    Public PingsPerPixel As Integer = 1
#End Region

    ' Statistics
    Dim Points() As System.Drawing.PointF
    Dim AvgY As Single
    Dim AvgPing As Single
    Dim TotalPingTime As TimeSpan
    Dim PingsPerSecond As Double
    Dim PingsString As String
    Public Sub Draw()
        Points = GetPoints()

        If Points.Length > 1 Then
            If Dimensions.Width > 200 Then
                DrawModeGraph()
            Else
                DrawModeFading()
            End If
        End If

        TotalPingTime = Now - Ping.PingStarted
        PingsPerSecond = Ping.PingTimes.Count / TotalPingTime.TotalSeconds
        PingsString = PingsPerSecond.ToString("0.0000") & " p/s"
        _Graphics.DrawString(Ping.Address, PingFont, PingBrush, New PointF(CSng(1), CSng(0)))
        _Graphics.DrawString(AvgPing.ToString("0.00") & " ms", PingFont, PingBrush, New PointF(CSng(1), CSng(13)))
        _Graphics.DrawString(PingsString, PingFont, PingBrush, New PointF(CSng(1), CSng(25)))
    End Sub

    ' STATS
    Public NumAverages As Integer = 12
    Public PingTotals(NumAverages - 1) As Integer ' Sum of respective pingtimes 
    Public PingAverages(NumAverages - 1) As Single ' The averages over different timespans
    Public AveragesPoints(NumAverages - 1) As PointF
    Sub DrawModeGraph()
        ' Set high quality rendering
        SetQuality(True)
        ' CLEAR
        _Graphics.Clear(BGColor)


        ' DRAW ping graph
        _Graphics.DrawLines(GraphPen, Points)
        DrawGrid()

        '' Calculate all 12 averages
        'CalculateAverages()

        '' DRAW averages
        'AveragesPoints = GetAveragesPoints()
        '_Graphics.DrawLines(AvgPen, AveragesPoints)
        'For i As Integer = 0 To NumAverages - 2
        '    Try
        '        _Graphics.DrawString(PingAverages(i).ToString("0.0"), PingFont, Brushes.White, AveragesPoints(i).X, AveragesPoints(i).Y - 13)
        '    Catch ex As Exception

        '    End Try
        'Next
    End Sub

    Sub DrawModeFading()
        ' Set low quality rendering
        SetQuality(False)

        ' FADE
        If Points.Last.Y > 0 Then
            _Graphics.FillRectangle(BGSlowFadeBrush, Dimensions)
        Else
            _Graphics.FillRectangle(BGSlowFadeAlertBrush, Dimensions)
        End If

        ' DRAW current ping
        _Graphics.DrawLine(CurrentPen, CSng(0), Points.Last.Y, CSng(Dimensions.Width - 1), Points.Last.Y)
    End Sub

    Sub DrawGrid()
        For Y As Integer = 0 To Dimensions.Height Step 10
            _Graphics.DrawLine(GridPen, 0, Dimensions.Height - Y, Dimensions.Width - 1, Dimensions.Height - Y)
        Next
    End Sub

    Function GetPoints() As PointF()
        ' Get points to draw on graph
        Dim TotalPingCount As Integer = Ping.PingTimes.Count

        ' Restrict point count to window size
        Dim DrawablePingCount As Integer
        Dim MaxDrawablePings As Integer = Dimensions.Width * PingsPerPixel + 2
        DrawablePingCount = IIf(TotalPingCount < MaxDrawablePings, TotalPingCount, MaxDrawablePings)

        Dim Points(DrawablePingCount - 1) As System.Drawing.PointF
        Dim PingTime As Single = 0
        Dim PingSum As Integer = 0
        For i As Integer = 0 To DrawablePingCount - 1
            ' X
            Points(i).X = CSng(Dimensions.Width - (DrawablePingCount / PingsPerPixel) + i / PingsPerPixel + 1)

            ' Y
            PingTime = Ping.PingTimes(Ping.PingTimes.Count - DrawablePingCount + i)
            Points(i).Y = Dimensions.Height - PingTime

            ' Total in window
            PingSum += PingTime
        Next

        AvgPing = CSng(PingSum / DrawablePingCount)

        Return Points
    End Function

    Sub CalculateAverages()
        ' reset totals
        For i As Integer = 0 To NumAverages - 1
            PingTotals(i) = 0
        Next

        ' tally ping times
        Dim CurrentPing As Integer
        Dim MaxIndex = 2 ^ NumAverages - 1 ' 2^12 - 1...
        MaxIndex = IIf(Ping.PingTimes.Count > MaxIndex, MaxIndex, Ping.PingTimes.Count - 1)
        For i As Integer = 0 To MaxIndex
            CurrentPing = Ping.PingTimes(Ping.PingTimes.Count - i - 1)

            For j As Integer = 1 To NumAverages
                If i < 2 ^ j Then
                    PingTotals(j - 1) += CurrentPing
                End If
            Next
        Next

        ' divide ping times by ping counts
        Dim DivisionFactor As Integer
        Dim CurrentFactor As Integer
        For i As Integer = 1 To NumAverages
            CurrentFactor = 2 ^ i
            If CurrentFactor > Ping.PingTimes.Count Then ' less actual pings than what we're taking the average of
                DivisionFactor = Ping.PingTimes.Count
            Else
                DivisionFactor = CurrentFactor
            End If
            PingAverages(i - 1) = PingTotals(i - 1) / DivisionFactor
        Next
    End Sub

    Function GetAveragesPoints() As PointF()
        Dim PointRangeMin As Integer = 1
        Dim PointRangeMax As Integer = NumAverages - 1
        Dim Points(PointRangeMax - PointRangeMin) As PointF
        Dim PointSpacing As Single = (Dimensions.Width + 1) / (PointRangeMax - PointRangeMin)
        For i As Integer = PointRangeMin To PointRangeMax
            ' reverse, right to left
            Points(i - PointRangeMin) = New PointF(PointSpacing * (PointRangeMax - i) - 1, Dimensions.Height - PingAverages(i))
        Next
        Return Points
    End Function

    Sub SetQuality(ByVal High As Boolean)
        If High Then
            If _Graphics.SmoothingMode = Drawing2D.SmoothingMode.None Then
                _Graphics.SmoothingMode = Drawing2D.SmoothingMode.HighQuality
                _Graphics.CompositingQuality = Drawing2D.CompositingQuality.HighQuality
            End If
        Else
            If Not (_Graphics.SmoothingMode = Drawing2D.SmoothingMode.None) Then
                _Graphics.SmoothingMode = Drawing2D.SmoothingMode.None
                _Graphics.CompositingQuality = Drawing2D.CompositingQuality.HighSpeed
            End If
        End If
    End Sub

    Public ReadOnly Property GraphBitmap As Bitmap
        Get
            Return _GraphBitmap
        End Get
    End Property
End Module
