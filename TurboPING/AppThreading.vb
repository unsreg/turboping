﻿Module AppThreading
    Public PingThreadResult As Integer = 0

    Sub StartPingThread()
        Dim a As New Action(AddressOf PingThread)
        Dim t As New Threading.Tasks.Task(a)
        t.Start()
    End Sub

    Sub PingThread()
        While True
            System.Threading.Thread.Sleep(1000)
            PingThreadResult = 1
            System.Threading.Thread.Sleep(1000)
            PingThreadResult = 0
        End While
    End Sub
End Module
