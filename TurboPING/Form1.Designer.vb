﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.GraphBox = New System.Windows.Forms.PictureBox()
        Me.PingTimer = New System.Windows.Forms.Timer(Me.components)
        Me.PingSpeedControl = New System.Windows.Forms.NumericUpDown()
        Me.TextBoxAddress = New System.Windows.Forms.TextBox()
        Me.PanelPingChannelControl = New System.Windows.Forms.Panel()
        Me.NumericUpDownWidth = New System.Windows.Forms.NumericUpDown()
        CType(Me.GraphBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PingSpeedControl, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelPingChannelControl.SuspendLayout()
        CType(Me.NumericUpDownWidth, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.Text = "NotifyIcon1"
        Me.NotifyIcon1.Visible = True
        '
        'GraphBox
        '
        Me.GraphBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GraphBox.BackColor = System.Drawing.Color.Black
        Me.GraphBox.Location = New System.Drawing.Point(0, 0)
        Me.GraphBox.Name = "GraphBox"
        Me.GraphBox.Size = New System.Drawing.Size(974, 467)
        Me.GraphBox.TabIndex = 0
        Me.GraphBox.TabStop = False
        '
        'PingTimer
        '
        Me.PingTimer.Enabled = True
        Me.PingTimer.Interval = 250
        '
        'PingSpeedControl
        '
        Me.PingSpeedControl.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PingSpeedControl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PingSpeedControl.Font = New System.Drawing.Font("Consolas", 9.75!)
        Me.PingSpeedControl.Location = New System.Drawing.Point(305, -2)
        Me.PingSpeedControl.Maximum = New Decimal(New Integer() {100000, 0, 0, 0})
        Me.PingSpeedControl.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.PingSpeedControl.Name = "PingSpeedControl"
        Me.PingSpeedControl.Size = New System.Drawing.Size(59, 23)
        Me.PingSpeedControl.TabIndex = 1
        Me.PingSpeedControl.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.PingSpeedControl.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'TextBoxAddress
        '
        Me.TextBoxAddress.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBoxAddress.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TextBoxAddress.Font = New System.Drawing.Font("Consolas", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBoxAddress.Location = New System.Drawing.Point(83, 0)
        Me.TextBoxAddress.Name = "TextBoxAddress"
        Me.TextBoxAddress.Size = New System.Drawing.Size(221, 16)
        Me.TextBoxAddress.TabIndex = 2
        Me.TextBoxAddress.Text = "google.com"
        Me.TextBoxAddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'PanelPingChannelControl
        '
        Me.PanelPingChannelControl.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanelPingChannelControl.BackColor = System.Drawing.Color.White
        Me.PanelPingChannelControl.Controls.Add(Me.NumericUpDownWidth)
        Me.PanelPingChannelControl.Controls.Add(Me.TextBoxAddress)
        Me.PanelPingChannelControl.Controls.Add(Me.PingSpeedControl)
        Me.PanelPingChannelControl.Location = New System.Drawing.Point(572, 0)
        Me.PanelPingChannelControl.Name = "PanelPingChannelControl"
        Me.PanelPingChannelControl.Size = New System.Drawing.Size(402, 17)
        Me.PanelPingChannelControl.TabIndex = 3
        '
        'NumericUpDownWidth
        '
        Me.NumericUpDownWidth.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.NumericUpDownWidth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumericUpDownWidth.Font = New System.Drawing.Font("Consolas", 9.75!)
        Me.NumericUpDownWidth.Location = New System.Drawing.Point(361, -2)
        Me.NumericUpDownWidth.Maximum = New Decimal(New Integer() {100000, 0, 0, 0})
        Me.NumericUpDownWidth.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDownWidth.Name = "NumericUpDownWidth"
        Me.NumericUpDownWidth.Size = New System.Drawing.Size(44, 23)
        Me.NumericUpDownWidth.TabIndex = 3
        Me.NumericUpDownWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDownWidth.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'FormMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(974, 467)
        Me.Controls.Add(Me.PanelPingChannelControl)
        Me.Controls.Add(Me.GraphBox)
        Me.MaximizeBox = False
        Me.Name = "FormMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TurboPING"
        Me.TopMost = True
        CType(Me.GraphBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PingSpeedControl, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelPingChannelControl.ResumeLayout(False)
        Me.PanelPingChannelControl.PerformLayout()
        CType(Me.NumericUpDownWidth, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents GraphBox As System.Windows.Forms.PictureBox
    Friend WithEvents PingTimer As System.Windows.Forms.Timer
    Friend WithEvents PingSpeedControl As System.Windows.Forms.NumericUpDown
    Friend WithEvents TextBoxAddress As System.Windows.Forms.TextBox
    Friend WithEvents PanelPingChannelControl As System.Windows.Forms.Panel
    Friend WithEvents NumericUpDownWidth As System.Windows.Forms.NumericUpDown

End Class
