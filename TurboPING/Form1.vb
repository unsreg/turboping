﻿Public Class FormMain
    Private BorderWidth, TitleBarHeight As Integer

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Application.AddMessageFilter(New MyAppFilter(Me))
        BorderWidth = GetBorderWidth()
        TitleBarHeight = GetTitleHeight()

        Settings.LoadSettings()
        TextBoxAddress.Text = Ping.Address
        Graph.UpdateDimensions()
        PaintGraph()

        ' Create and start ping threads
        'AppThreading.StartPingThread()
    End Sub

    Public Sub FormTurboPING2_KeyDown(ByVal Shift As Boolean, ByVal e As System.Windows.Forms.KeyEventArgs)
        Dim ShiftPixels As Integer
        ShiftPixels = IIf(Shift, 10, 1)

        Select Case e.KeyCode
            Case Keys.Left
                Me.Left -= ShiftPixels
            Case Keys.Right
                Me.Left += ShiftPixels
            Case Keys.Up
                Me.Top -= ShiftPixels
            Case Keys.Down
                Me.Top += ShiftPixels
        End Select
    End Sub

    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Settings.SaveSettings()
    End Sub

    Private Sub Form1_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Graph.UpdateDimensions()
        PaintGraph()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles PingTimer.Tick
        PingTimer.Stop()
        Ping.Measure()
        PaintGraph()
        PingTimer.Start()
    End Sub

    Sub PaintGraph()
        Graph.Draw()
        GraphBox.Image = Graph.GraphBitmap
        GraphBox.Update()
    End Sub

    Private Sub NumericUpDown1_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PingSpeedControl.ValueChanged
        Try
            PingTimer.Interval = PingSpeedControl.Value
        Catch ex As Exception
        End Try
    End Sub

    Private Sub NumericUpDownWidth_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NumericUpDownWidth.ValueChanged
        Graph.PingsPerPixel = NumericUpDownWidth.Value
    End Sub

    Private Sub TextBoxAddress_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBoxAddress.LostFocus
        Dim NewAddress As String = TextBoxAddress.Text
        If Ping.Address <> NewAddress Then
            PingTimer.Stop()
            If Ping.Test(NewAddress) Then
                Ping.Address = NewAddress
                TextBoxAddress.BackColor = Color.White
                PingTimer.Start()
            Else
                TextBoxAddress.BackColor = Color.Red
            End If
        End If
    End Sub

    Private Sub GraphBox_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GraphBox.Click
        SwitchFormState()
    End Sub

#Region "Form State code"
    Sub SwitchFormState()
        If Me.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable Then
            Me.Top += BorderWidth + TitleBarHeight
            Me.Left += BorderWidth
            Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        Else
            Me.Top -= BorderWidth + TitleBarHeight
            Me.Left -= BorderWidth
            Me.FormBorderStyle = Windows.Forms.FormBorderStyle.Sizable
        End If

        'toggle channel control panel visibility
        PanelPingChannelControl.Visible = Not PanelPingChannelControl.Visible
    End Sub

    Function GetBorderWidth() As Integer
        Return CInt((Me.Width - Me.ClientSize.Width) / 2)
    End Function

    Function GetTitleHeight() As Integer
        Return Me.Height - Me.ClientSize.Height - 2 * GetBorderWidth()
    End Function
#End Region
End Class
