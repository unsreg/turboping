﻿Public Class MyAppFilter
    Implements IMessageFilter

    Const WM_KEYDOWN As Integer = &H100
    Const WM_KEYUP As Integer = &H101


    Private _Parent As FormMain
    Sub New(ByRef Parent As FormMain)
        _Parent = Parent
    End Sub

    Private _Shift As Boolean = False
    Public Capture As Boolean = False
    Public Function PreFilterMessage(ByRef m As System.Windows.Forms.Message) As Boolean Implements System.Windows.Forms.IMessageFilter.PreFilterMessage
        If Not _Parent.PanelPingChannelControl.Visible Then
            Select Case m.Msg
                Case WM_KEYDOWN
                    Select Case m.WParam.ToInt32
                        Case Keys.ShiftKey
                            _Shift = True
                    End Select
                    Dim k As New KeyEventArgs(CType(m.WParam.ToInt32, System.Windows.Forms.Keys))
                    _Parent.FormTurboPING2_KeyDown(_Shift, k)
                    Return True
                Case WM_KEYUP
                    Select Case m.WParam.ToInt32
                        Case Keys.ShiftKey
                            _Shift = False
                    End Select
                    Return True
            End Select
        End If

        Return False
    End Function
End Class
